# Soal Shift Sisop Modul 2 2022

### Anggota Kelompok
| Nama                       | NRP        |
| :------------------------- | :----------|
| Marcellino Mahesa Janitra  | 5025201105 |
| Afiq Akram                 | 5025201270 |
| Samuel Berkat Hulu         | 5025201055 |

---

## Solusi 
### Soal 1
#### 1A
Pertama kita diminta untuk mendownload file yang menjadi data untuk melakukan proses gacha. Kodenya sebagai berikut
```c
void downld(char *url, char *filename){
    pid_t id_child;
    id_child = fork();
    
    if(id_child == 0){
        char *argv[] = {"wget","-q", "--no-check-certificate", url, "-O", filename, NULL};
        execv("/bin/wget", argv);
        exit(EXIT_SUCCESS);
    }
    else if(id_child > 0) {
        wait(NULL);
    }
}
```
Fungsi :
1. `pid_t id_child` berfungsi untuk mengembalikan process ID dari proses saat ini.
2. `id_child = fork()` forking kepada id proses yang berjalan.
3. `if(id_child == 0)` jika proses adalah `child`, maka akan masuk `if`.
4. `char *argv[]` untuk menampung kata-kata sesuai yang tertulis command line untu dijadikan parameter pada fungsi `execv()`.
5. `execv("/bin/wget, argv")` menjalankan fungsi `wget` dengan parameter yang ada di variable `argv`.
6. `else if (id_child > 0)` jika parent, maka masuk else.
7. `wait(NULL)` adalah sintaks untuk menunggu hingga child selesai mengeksekusi programnya.

File yang telah didownload akan diunzip sesuai dengan ketentuan soal. <br>
Berikut adalah kode untuk meng-unzip dari nama file yang diberikan.
```c
void unzp(char *filename){
    pid_t id_child;
    id_child = fork();
    
    if (id_child == 0) {
        char *argv[] = {"unzip", "-n", "-q", filename, NULL};
        execv("/bin/unzip", argv);
        exit(EXIT_SUCCESS);
    }
    else if (id_child > 0) {
        wait(NULL);
    }
}

Selanjutnya, kita diminta untuk membuat direktori yang bernama `gacha_gacha`.
Berikut adalah kode untuk membuat direktori.
```c
void create_dir(char *dirname) {
    pid_t id_child;
    id_child = fork();
    
    if (id_child == 0) {
        DIR *dir = opendir(dirname);
        if (dir)
        {
            closedir(dir);
        } else {
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/bin/mkdir",argv);
        }
        exit(EXIT_SUCCESS);
    }
    else if (id_child > 0) {
        wait(NULL);
    }
}
```
#### 1B, 1C, dan 1D
Berikut adalah kode untuk program gacha pada `int main()`.
```c
int total_gacha = 0;
while (1) {
        time_t timer;
        char template_time[100];
        struct tm *tm_info;
        timer = time(NULL);
        tm_info=localtime(&timer);
        strftime(template_time, 100, "%H:%M:%S", tm_info);
        int round_ten = tot_gacha - (tot_gacha%10);
        char number[5], cur_dir[100];
        sprintf(number,"%d",round_ten);
        strcpy(cur_dir,"gacha_gacha/");
        strcat(cur_dir,template_time);
        strcat(cur_dir, "_gacha_");
        strcat(cur_dir, number);
        strcat(cur_dir, ".txt");

    for (size_t i = 0; i < 10; i++)
        {
            tot_gacha++;
            prims(&primogems);
            gacha(tot_gacha, cur_dir);
        }
        sleep(1);
        srand(time(NULL));
    }
}

```
1. `char cur_dir[100]` untuk membuat format direktori sesuai ketentuan `tot_gacha{jumlah-gacha}`.
2. `sprintf(num_dir, "%d", round_nine)` memformat int `round_nine` kedalam string lalu disimpan didalam variable `num_dir`.
3. `for (int j = 0; j < 9; j++)` untuk looping per 1 folder (kelipatan 90).
4. `time_t timer` untuk menggunakan fungsi `localtime()` kedepannya.
5. `timer = time(NULL)` inisialisasi timer.
6. `struct tm *tm` struct untuk memformat time menjadi struct
7. `strftime(template_time, 100, "%H:%M:%S", tm_info)` memformat `datetime` sesuai format soal dan menyimpannya kedalam `formatted_time`.
8. `for (int i = 0; i < 10; i++)` untuk looping per 10 gacha yang akan dimasukkan kedalam 1 file .txt.
9. `srand(time(NULL))` untuk mereset `rand()` (didalam function `g_rand()`).

Fungsi ini akan dijalankan setiap kali fungsi `gacha()` akan dipanggil. Fungsi ini akan mengurangi `primogems` hingga dibawah batas gacha yaitu 160. <br>
Jika nilai `primogems` dibawah 160, maka program akan menunggu hingga waktu yang ditentukan. Penjelasan lebih lanjut akan diberikan di poin 1e.

Untuk kode dari function `gacha()` adalah sebagai berikut.
```c

void gacha(int count, char cur_dir[100]){ 

    int max;
    char path[100];
    DIR *dp;
    struct dirent *ep;
    char path_r[100];
    char ty_item[100];

    if(count%2==0){
        max = 130;
        strcpy(ty_item,"Weapons");
        strcpy(path, "weapons/");
    } else {
        max = 48;
        strcpy(ty_item,"Characters");
        strcpy(path, "characters/");
    } 
    dp = opendir(path);

    int random = g_rand(max);
    if(dp != NULL)
    {
        for (int j = 0; j <= random; j++){
            ep = readdir(dp);
        }
        strcpy(path_r, ep->d_name);
        ep = NULL;
        closedir(dp);
    } else {
        perror("Can't Open Directory");
    }
    // Baca json
    strcat(path, path_r);
    char buffer[4096];
    struct json_object *parsed_json;
    struct json_object *rarity;
    struct json_object *name;
    FILE *fp;

    fp = fopen(path, "r");
    fread(buffer, 4096, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    FILE *gacha_file;
    char insert_into_file[100];
    char num[5], primo[10];
    sprintf(num, "%d",count);
    sprintf(primo, "%d",primogems);

    gacha_file = fopen(cur_dir, "a");

    strcpy(insert_into_file,num);
    strcat(insert_into_file,"_");
    strcat(insert_into_file,ty_item);
    strcat(insert_into_file,"_");
    strcat(insert_into_file,json_object_get_string(rarity));
    strcat(insert_into_file,"_");
    strcat(insert_into_file,json_object_get_string(name));
    strcat(insert_into_file,"_");
    strcat(insert_into_file,primo);
    strcat(insert_into_file,"\n");

    fputs(insert_into_file,gacha_file);
    fclose(gacha_file);
}

```
Dengan parameter `count` berupa total gacha yang sudah diakukan dan `cur_dir[100]` untuk path ke file yang diinginkan.
- Variable `max`, `item_type`, dan `path` merupakan jumlah file yang ada di folder `weapons/` ataupun `characters/`. Nilai kedua variable tersebut akan diisi bergantung dari variable `count`. Jika `count` adalah genap, maka set dengan ketentuan dari `Weapons`. Dan jika genap, isi dengan ketentuan dari `Characters`.
- `max` adalah jumlah dari file pada path tertentu (untuk `weapons/` adalah 130, dan untuk `characters/` adalah 48). Ditentukan secara manual.
- Terdapat `struct json_object` untuk menampung hasil json dari file .json yang diambil. Terdapat juga beberapa fungsi header `<json-c/json.h>` seperti mengambil hasil, mem-parse string json, dan lainnya.
- Pada bagian terakhir dari kode berfungsi untuk menuliskan hasil gacha sesuai format `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}` kedalam file dengan path berdasarkan variable `cur_dir`

Penamaan path untuk folder dan file terdapat pada `int main()` yang nantinya akan dipassing kedalam fungsi `gacha()`. <br>
Untuk kode penamaan folder/file adalah sebagai berikut.
```c
while (1)
{
    // create dir name
    char cur_dir[100], num_dir[5];
    strcpy(cur_dir, "gacha_gacha/");
    int round_nine = total_gacha - (total_gacha % 90);
    sprintf(num_dir, "%d", round_nine);
    strcat(cur_dir, "tot_gacha");
    strcat(cur_dir, num_dir);
    strcat(cur_dir, "/");
    create_dir(cur_dir);

    for (int j = 0; j < 9; j++)
    {
        // create filename
        time_t timer;
        char formatted_time[100], number[5];
        char temp_dir[100];
        strcpy(temp_dir, cur_dir);
        struct tm *tm_info;
        timer = time(NULL);
        tm_info = localtime(&timer);
        strftime(formatted_time, 100, "%H:%M:%S", tm_info);
        int round_to_10 = total_gacha - (total_gacha % 10);
        sprintf(number, "%d", round_to_10);

    strcat(temp_dir, formatted_time);
    strcat(temp_dir, "_gacha_");
    strcat(temp_dir, number);
    strcat(temp_dir, ".txt");

        // ...
    }
}
```
Sesuai letak comment, blok kode penamaan folder/file terletak di bawah comment tersebut.

### 1e

Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)
 <br>
```c

void zp_fd(){
    pid_t id_child;
    id_child = fork();
    
    if (id_child == 0) {
        char *argv[] = {"zip", "-q", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
        execv("/bin/zip",argv);
        exit(EXIT_SUCCESS);
    }
    else if (id_child > 0) {
        wait(NULL);
    }
}

```
Fungsi `wait_date()` adalah untuk membuat program menunggu hingga waktu yang ditentukan, yaitu `30 Maret jam 04:44`. <br>
Sedangkan cuplikan kode pada fungsi `cek_primo()` membuat program menunggu hingga 3 jam setelah program dimulai.

Pada akhir program, semua folder dan file dalam folder `gacha_gacha/` akan di-zip dan dipassword sesuai ketentuan soal. <br>
Untuk kode zip adalah sebagai berikut.
```c
void zp_fd(){
    pid_t id_child;
    id_child = fork();
    
    if (id_child == 0) {
        char *argv[] = {"zip", "-q", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
        execv("/bin/zip",argv);
        exit(EXIT_SUCCESS);
    }
    else if (id_child > 0) {
        wait(NULL);
    }
}

```




### Soal 2
1. Unzip file penting ke folder home

```c 
char* home_dir = info->pw_dir;
    if (id == 0) {
        int id2 = fork();
        int status2;
        if (id2 == 0) {
            mkdir(strcat(home_dir,"/shift2/drakor"));
        } else {
            while((wait(&status2)) > 0);
            char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", strcat(home_dir,"/temp/"), NULL};
            execv("/bin/unzip", argv);
        }
    } else {
        while ((wait(&status)) > 0);
    }
```
foto yang di ekstrak akan di unzip ke folder temp dulu, nantinya akan dipindah ke masing masing genre folder 

2. membuat folder sesuai kategori yang ada
- meng-read semua file foto dalam folder temp
```c 
DIR *dp;
    struct dirent *ep;
    snprintf(folder, 300, "%s%s", home_dir, "/temp");
    dp = opendir(folder);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
```
- split berdasarkan karakter ";", simpan data penting seperti tahun dan judul per line
```c 
 char file_path[300];
                snprintf(file_path, 300, "%s/temp/%s", home_dir, ep->d_name);
                char* component = strtok(ep->d_name, ";");
                char title[128];
                snprintf(title, 128, "%s.png", component);
                int flag = 1;
                char tahun_rilis[5];
```
- read token yang sudah di split tadi
```c 
  while(component != NULL) {
      char* genre = component;
      component = strtok(NULL, ";");
      if(flag) {
          snprintf(tahun_rilis, 5, "%s", component);
          flag = 0;
      }
```
- bikin folder genre per genre, lalu copy file jika:
- 1. karakter terakhir dalam line
 ```c
   if (component == NULL) {
      int genre_process = fork();
      if (genre_process == 0) {
          snprintf(folder, 128, "%s/shift2/drakor/", home_dir);
          char *genre_folder = folder;
          strcat(genre_folder, strtok(genre, "."));
          mkdir(genre_folder);
          char destination[300];
          snprintf(destination, 300, "%s/%s", genre_folder, title);
          catat_film(tahun_rilis, title, genre, genre_folder);
          cp(file_path, destination);
      } else {
          while ((wait(&status)) > 0);
      }
  }
 ```
- 2. karakter berisi "_" (berarti punya dua genre)
 ```c 
   if (strstr(genre, "_")) {
      int process2 = fork();
      if (process2 == 0) {
          char* poster = strtok(genre, "_");
          char genre2[10];
          snprintf(genre2, 10, "%s", poster);
          poster = strtok(NULL, "_");
          snprintf(folder, 128, "%s/shift2/drakor/", home_dir);
          char *genre_folder = folder;
          strcat(genre_folder, genre2);
          mkdir(genre_folder);
          char destination[300];
          snprintf(destination, 300, "%s/%s.png", genre_folder, title);
          catat_film(tahun_rilis, title, genre, genre_folder);
          cp(file_path, destination);
      } else {
          while ((wait(&status)) > 0);
      }
  }
 ```

- 3. function mkdir, cp, dan catat film yang dipakai:
 ```c 
 void mkdir(char* path)
{
    int id = fork();
    int status;
    if (id == 0) {
        char *argv[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argv);
    } else {
        while ((wait(&status)) > 0);
    }
}

void cp(char* source, char* dest)
{
    char *argv[] = {"cp", source, dest, NULL};
    execv("/bin/cp", argv);
}

void catat_film(char* tahun, char* judul, char* genre, char* path)
{
    char tmp[256];
	strcpy(tmp, path);
    strcat(tmp, "/data.txt");
    if (access(tmp, F_OK ) != 0) {
        FILE *fp;
        fp = fopen (tmp, "a+");
        fprintf(fp, "kategori : %s\n\n", genre);
        fclose(fp);
    }
    FILE *fp;
	fp = fopen (tmp, "a+");
	fprintf(fp, "nama : %s\nrilis : %s\n\n", judul, tahun);
	fclose(fp);

}
 ```

3. hapus file temp yang lama, setelah semua filenya sudah di copy ke folder genre tujuan
```c 
    snprintf(folder, 128, "%s/temp", home_dir);
    char *argv[] = {"rm", "-fr", folder, NULL};
    execv("/bin/rm", argv);
```

### Hasil Output
1. hasil create gnere folder
![image](https://media.discordapp.net/attachments/860285754155728896/957652832637575188/unknown.png?width=1191&height=670)

2. hasil unzip
![image](https://media.discordapp.net/attachments/860285754155728896/957652856108892200/unknown.png?width=1191&height=670)
   
3. hasil pencatatat ke data.txt
![image](https://cdn.discordapp.com/attachments/860285754155728896/957654819387084810/unknown.png)
### Soal 3
1. buat 2 directory, darat.. lalu 3 detik kemudian air
```c 
    struct passwd *info = getpwuid(1000);
    if (id == 0) {
        int id2 = fork();
        if (id2 == 0) {
            char *argv[] = {"mkdir", "-p", strcat(info->pw_dir,"/modul2/darat"), NULL};
            execv("/bin/mkdir", argv);
        } else {
            sleep(3);
            char *argv[] = {"mkdir", "-p", strcat(info->pw_dir,"/modul2/air"), NULL};
            execv("/bin/mkdir", argv);
        }
    } else {
        while ((wait(&status)) > 0);
    }
```
2. unzip file, masing masing dikelompokan berdasarkan jenis, air atau darat
```c 
    int id3 = fork();
    int status3;
    // printf("unzipping\n");
    if (id3 == 0) {
        int id4 = fork();
        int status4;
        if (id4 == 0) {
            char *argv[] = {"unzip", "-j", "soal3/animal.zip", "*darat*", "-d", strcat(info->pw_dir,"/modul2/darat"), NULL};
            execv("/bin/unzip", argv);
        } else {
            while ((wait(&status4)) > 0);
            char *argv[] = {"unzip", "-j", "soal3/animal.zip", "*air*", "-d", strcat(info->pw_dir,"/modul2/air"), NULL};
            execv("/bin/unzip", argv);
        }
    } else {
        while ((wait(&status3)) > 0);
    }
```
3. hapus hewan yang mempunyai karakter "bird"
```c 
    int id5 = fork();
    if (id5 == 0) {
        char *path = strcat(info->pw_dir,"/modul2/darat/*bird*");
        char *argv[] = {"rm", path, NULL};
        execv("/bin/rm", argv);
    } else {
        while ((wait(&status3)) > 0);
        DIR *dp;
        struct dirent *ep;
        dp = opendir(strcat(info->pw_dir,"/modul2/darat"));

        if (dp != NULL)
        {
            while ((ep = readdir (dp))) {
                puts (ep->d_name);
            }
            closedir (dp);
        }
    }
```
