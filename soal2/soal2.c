#include <stdio.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#include <string.h>
#include <wait.h>
#include <stdlib.h>
#include <dirent.h>

void mkdir(char* path)
{
    int id = fork();
    int status;
    if (id == 0) {
        char *argv[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", argv);
    } else {
        while ((wait(&status)) > 0);
    }
}

void cp(char* source, char* dest)
{
    char *argv[] = {"cp", source, dest, NULL};
    execv("/bin/cp", argv);
}

void catat_film(char* tahun, char* judul, char* genre, char* path)
{
    char tmp[256];
	strcpy(tmp, path);
    strcat(tmp, "/data.txt");
    if (access(tmp, F_OK ) != 0) {
        FILE *fp;
        fp = fopen (tmp, "a+");
        fprintf(fp, "kategori : %s\n\n", genre);
        fclose(fp);
    }
    FILE *fp;
	fp = fopen (tmp, "a+");
	fprintf(fp, "nama : %s\nrilis : %s\n\n", judul, tahun);
	fclose(fp);

}

int main()
{
    int id = fork();
    int status;
    struct passwd *info = getpwuid(1000);
    // char* home_dir = strcat(info->pw_dir, "/soal-shift-sisop-modul-2-D12-2022/soal2");
    char* home_dir = info->pw_dir;
    if (id == 0) {
        int id2 = fork();
        int status2;
        if (id2 == 0) {
            mkdir(strcat(home_dir,"/shift2/drakor"));
        } else {
            while((wait(&status2)) > 0);
            char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", strcat(home_dir,"/temp/"), NULL};
            execv("/bin/unzip", argv);
        }
    } else {
        while ((wait(&status)) > 0);
    }
    char folder[300];
    DIR *dp;
    struct dirent *ep;
    snprintf(folder, 300, "%s%s", home_dir, "/temp");
    dp = opendir(folder);
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if ( !strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..") ) {}
            else {
                char file_path[300];
                snprintf(file_path, 300, "%s/temp/%s", home_dir, ep->d_name);
                char* component = strtok(ep->d_name, ";");
                char title[128];
                snprintf(title, 128, "%s.png", component);
                int flag = 1;
                char tahun_rilis[5];
                while(component != NULL) {
                    char* genre = component;
                    component = strtok(NULL, ";");
                    if(flag) {
                        snprintf(tahun_rilis, 5, "%s", component);
                        flag = 0;
                    }

                    if (component == NULL) {
                        int genre_process = fork();
                        if (genre_process == 0) {
                            snprintf(folder, 128, "%s/shift2/drakor/", home_dir);
                            char *genre_folder = folder;
                            strcat(genre_folder, strtok(genre, "."));
                            mkdir(genre_folder);
                            char destination[300];
                            snprintf(destination, 300, "%s/%s", genre_folder, title);
                            catat_film(tahun_rilis, title, genre, genre_folder);
                            cp(file_path, destination);
                        } else {
                            while ((wait(&status)) > 0);
                        }
                    }
                    if (strstr(genre, "_")) {
                        int process2 = fork();
                        if (process2 == 0) {
                            char* poster = strtok(genre, "_");
                            char genre2[10];
                            snprintf(genre2, 10, "%s", poster);
                            poster = strtok(NULL, "_");
                            snprintf(folder, 128, "%s/shift2/drakor/", home_dir);
                            char *genre_folder = folder;
                            strcat(genre_folder, genre2);
                            mkdir(genre_folder);
                            char destination[300];
                            snprintf(destination, 300, "%s/%s.png", genre_folder, title);
                            catat_film(tahun_rilis, title, genre, genre_folder);
                            cp(file_path, destination);
                        } else {
                            while ((wait(&status)) > 0);
                        }
                    }
                }
            }
        }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");

    snprintf(folder, 128, "%s/temp", home_dir);
    char *argv[] = {"rm", "-fr", folder, NULL};
    execv("/bin/rm", argv);
}
