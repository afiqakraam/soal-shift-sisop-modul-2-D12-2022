#include <stdio.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>

int main () {
    int id = fork();
    int status;
    struct passwd *info = getpwuid(1000);
    if (id == 0) {
        int id2 = fork();
        if (id2 == 0) {
            char *argv[] = {"mkdir", "-p", strcat(info->pw_dir,"/modul2/darat"), NULL};
            execv("/bin/mkdir", argv);
        } else {
            sleep(3);
            char *argv[] = {"mkdir", "-p", strcat(info->pw_dir,"/modul2/air"), NULL};
            execv("/bin/mkdir", argv);
        }
    } else {
        while ((wait(&status)) > 0);
    }
    int id3 = fork();
    int status3;
    // printf("unzipping\n");
    if (id3 == 0) {
        int id4 = fork();
        int status4;
        if (id4 == 0) {
            char *argv[] = {"unzip", "-j", "soal3/animal.zip", "*darat*", "-d", strcat(info->pw_dir,"/modul2/darat"), NULL};
            execv("/bin/unzip", argv);
        } else {
            while ((wait(&status4)) > 0);
            char *argv[] = {"unzip", "-j", "soal3/animal.zip", "*air*", "-d", strcat(info->pw_dir,"/modul2/air"), NULL};
            execv("/bin/unzip", argv);
        }
    } else {
        while ((wait(&status3)) > 0);
    }
    int id5 = fork();
    if (id5 == 0) {
        char *path = strcat(info->pw_dir,"/modul2/darat/*bird*");
        char *argv[] = {"rm", path, NULL};
        execv("/bin/rm", argv);
    } else {
        while ((wait(&status3)) > 0);
        DIR *dp;
        struct dirent *ep;
        dp = opendir(strcat(info->pw_dir,"/modul2/darat"));

        if (dp != NULL)
        {
            while ((ep = readdir (dp))) {
                puts (ep->d_name);
            }
            closedir (dp);
        }
    }
    return 0;
}
